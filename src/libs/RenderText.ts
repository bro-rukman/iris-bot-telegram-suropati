import { sprintf } from 'sprintf-js'

export function RenderText(
    recordset : any[], 
    title : string, 
    headers : string[], 
    columns : string[], 
    renderer: {} = {},
    delimiter = '|', 
    format = null
) {

    if(format) {
        let text = '<pre>' + title + '\n\n'        
        text += sprintf(format, ...headers) + '\n'
        text += recordset.map(record => sprintf(format, ...columns.map(column => renderer[column] ? renderer[column](record[column]) : record[column]))).join('\n')
        text += '</pre>'
        return text
    } else {
        let text = '<b>' + title + '</b>\n\n'
        text += '<b>' + headers.join(` ${delimiter} `) + '</b>\n'
        text += recordset.map(record => columns.map(column => renderer[column] ? renderer[column](record[column]) : record[column]).join(` ${delimiter} `)).join('\n')
        return text
    }
}