import Telegraf, { ContextMessageUpdate, Middleware } from 'telegraf'

export type CommandType = {
    name: string,
    description: string,
    handler: (ctx: ContextMessageUpdate) => Middleware<ContextMessageUpdate>
}

export function RegisterCommands(bot : Telegraf<ContextMessageUpdate>, botCommands : any[]) {
    const commands = []
    botCommands.forEach(cmd => {
        commands.push({name: cmd.name, description: cmd.description})
        bot.command('test',  (ctx) => ctx.reply('Hello'))
    })
    return commands
}